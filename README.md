# Lease Plan Assignment for Automation QA - Lakshmi Sushmitha Kolluru

## API used as usecase - [Open Skills API](https://any-api.com/dataatwork_org/dataatwork_org/docs/API_Description)

**Technology Detail**

- Java Development Environment - JDK 8 
- Maven 
- IDE Preference - Eclipse 
- BDD Framework - Serenity/Cucumber 6
- CI tool - Gitlab CI

## Installation Instructions

1. Install necessary software for local setup as mentioned in the technology detail section above
2. Clone the Git repository to your local workspace
3. Using command prompt, navigate to the directory where the source code is cloned. Alternatively, use an IDE like Eclipse
3. If Maven is installed, run `mvn clean compile` to verify a successful compilation of the source code
4. To run the test suite, run `mvn clean verify` to verify the execution status of the test cases and to generate a serenity test report
5. To open the serenity test reports, navigate to the folder "target/site/serenity" and open the file index.html in your browser

## Development and Contribution Instructions

1. All the test scripts are located in the folder "src/test/java"
2. The tests are written in BDD format using the standards of Cucumber/Gherkin
3. To create/update a feature file, Please refer to the folder "src/test/resources"
4. If you wish to add/change a folder for feature files, please update the entry in "AcceptanceTestSuite.java"
5. To create/update step definitions for features, please refer to the test scripts in the package "com.leaseplan.api.test.steps"
6. If you wish to add/change a package for step definitions, please update the entry in "AcceptanceTestSuite.java"
7. All Data Transfer Objects should only be placed in the package "com.leaseplan.api.test.steps.dto"
8. Since we use serenity framework, please adhere to the re-use/standards of step definitions

## API Interactions - Open Skills API

I chose this API as it provides an unauthenticated API with rich data related to job search information. 

This assignment automates the test scenarios and cases for the following endpoints:

- List Jobs endpoint - GET - [URL](https://any-api.com/dataatwork_org/dataatwork_org/docs/_jobs/GET)
- List Jobs by ID endpoint - GET - [URL](https://any-api.com/dataatwork_org/dataatwork_org/docs/_jobs_id_/GET)
- List Related Jobs endpoint - GET - [URL](https://any-api.com/dataatwork_org/dataatwork_org/docs/_jobs_id_related_jobs/GET)


Features and scenarios are described in the feature files using BDD formats hence avoiding duplicate documentation.

## CI Information

All the pipeline related information is documented in the gitlab-ci.yml file. As we do not intend to deploy any code, the current pipeline only runs the maven verify task and publishes the test reports to gitlab artifacts. 

To view the pipeline status, please visit the following URL [Gitlab pipelines](https://gitlab.com/skolluru/leaseplanapiassignment/-/pipelines) 

Test Reports from the CI Pipeline runs can be downloaded at the pipeline page using the artifact download link.
