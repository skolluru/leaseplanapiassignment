Feature: User is able to list related jobs 

	Scenario: User is able to list related jobs with a valid job_id
		Given a valid job_id "26bc4486dfd0f60b3bb0d8d64e001800" for the related jobs 
		When calling the related jobs end point with a valid job_id "26bc4486dfd0f60b3bb0d8d64e001800" 
		Then the response should list the jobs related to the given valid job_id "26bc4486dfd0f60b3bb0d8d64e001800"
		
	Scenario: User is not able to list related jobs with an invalid job_id
		Given an invalid job_id "26bc4486dfd0f60b3bb0d8d64eninja" for the related jobs
		When calling the related jobs end point with an invalid job_id "26bc4486dfd0f60b3bb0d8d64eninja" 
		Then the response should through an error message No job found matching the specified uuid "26bc4486dfd0f60b3bb0d8d64eninja"
		
	Scenario: User is not able to list related jobs with a blank job_id
		Given a blank job_id for the related jobs
		When calling the related jobs end point with a blank job_id
		Then the response should through an error message Cannot find job with id related_jobs
		