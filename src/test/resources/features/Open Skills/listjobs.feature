Feature: User is able to list jobs

	Scenario: User is able to list jobs without job_id
		Given API is available for listing jobs  
		When calling the list jobs end point 
		Then the response status code should be OK
		
	Scenario: User is able to list jobs with a valid job_id
		Given API is available for listing jobs with a valid job_id "26bc4486dfd0f60b3bb0d8d64e001800"
		When calling the list jobs end point with a valid job_id "26bc4486dfd0f60b3bb0d8d64e001800"
		Then the response should contain the job_id "26bc4486dfd0f60b3bb0d8d64e001800"
		
	Scenario: User is not able to list jobs with an invalid job_id
		Given API is available for listing jobs with an invalid job_id "26bc4486dfd0f60b3bb0d8d64e00R"
		When calling the list jobs end point with an invalid job_id "26bc4486dfd0f60b3bb0d8d64e00R"
		Then the response should through an error with a message Cannot find job with id "26bc4486dfd0f60b3bb0d8d64e00R"