package com.leaseplan.api.test.steps;

import static org.assertj.core.api.Assertions.assertThat;

import com.leaseplan.api.test.steps.dto.JobError;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class ListRelatedJobsByInValidJobIdStepDefinitions {
	Response jobresponse = null;
	RequestSpecification request = null;

	@Given("an invalid job_id {string} for the related jobs")
	public void given_listing_related_jobs(String id) {
		request = RestAssured.given();

	}

	@When("calling the related jobs end point with an invalid job_id {string}")
	public void when_call_list_related_jobs(String id) {
		jobresponse = request.get("http://api.dataatwork.org/v1/jobs/" + id + "/related_jobs");
	}

	@Then("the response should through an error message No job found matching the specified uuid {string}")
	public void then_validate_response_body(String id) {
		assertThat(jobresponse.statusCode()).isEqualTo(404);
		assertThat(jobresponse.contentType()).isEqualTo("application/json");
		
		JobError message = jobresponse.as(JobError.class);
		assertThat(message.getError().getMessage()).isEqualTo("No job found matching the specified uuid " + id);
	}

}
