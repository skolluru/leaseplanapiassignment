package com.leaseplan.api.test.steps;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import static org.assertj.core.api.Assertions.assertThat;

import com.leaseplan.api.test.steps.dto.Job;


public class ListJobsByValidJobIdStepDefinitions {
	Response jobresponse = null;
	RequestSpecification request = null;

	@Given("API is available for listing jobs with a valid job_id {string}")
	public void given_api_listing_jobs(String id) {
		request = RestAssured.given();

	}

	@When("calling the list jobs end point with a valid job_id {string}")
	public void when_call_list_jobs(String id) {
		jobresponse = request.get("http://api.dataatwork.org/v1/jobs/" + id);
	}

	@Then("the response should contain the job_id {string}")
	public void then_validate_response_code(String id) {
		assertThat(jobresponse.statusCode()).isEqualTo(200);
		assertThat(jobresponse.contentType()).isEqualTo("application/json");
		
		Job responseJob = jobresponse.as(Job.class);
		assertThat(responseJob.getUuid()).isEqualTo(id);
	}

}
