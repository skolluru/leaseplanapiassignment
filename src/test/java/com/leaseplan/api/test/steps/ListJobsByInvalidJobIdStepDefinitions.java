package com.leaseplan.api.test.steps;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import static org.assertj.core.api.Assertions.assertThat;

import com.leaseplan.api.test.steps.dto.JobError;

public class ListJobsByInvalidJobIdStepDefinitions {
	Response jobresponse = null;
	RequestSpecification request = null;

	@Given("API is available for listing jobs with an invalid job_id {string}")
	public void given_api_listing_jobs(String id) {
		request = RestAssured.given();

	}

	@When("calling the list jobs end point with an invalid job_id {string}")
	public void when_call_list_jobs(String id) {
		jobresponse = request.get("http://api.dataatwork.org/v1/jobs/" + id);
	}

	@Then("the response should through an error with a message Cannot find job with id {string}")
	public void then_validate_response_code(String id) {
		assertThat(jobresponse.statusCode()).isEqualTo(404);
		
		JobError message = jobresponse.as(JobError.class);
		assertThat(message.getError().getMessage()).isEqualTo("Cannot find job with id " + id);
	}

}
