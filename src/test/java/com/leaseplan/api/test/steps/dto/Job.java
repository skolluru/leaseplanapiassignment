package com.leaseplan.api.test.steps.dto;

public class Job {
	
	private	String normalized_job_title;
	private String parent_uuid;
	private String title;
	private String uuid;
	
	
	public String getNormalized_job_title() {
		return normalized_job_title;
	}
	public void setNormalized_job_title(String normalized_job_title) {
		this.normalized_job_title = normalized_job_title;
	}
	public String getParent_uuid() {
		return parent_uuid;
	}
	public void setParent_uuid(String parent_uuid) {
		this.parent_uuid = parent_uuid;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	
	

}
