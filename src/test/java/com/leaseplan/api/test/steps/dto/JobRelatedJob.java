package com.leaseplan.api.test.steps.dto;

public class JobRelatedJob {
	
	private String parent_uuid;
	private String title;
	private String uuid;
	public String getParent_uuid() {
		return parent_uuid;
	}
	public void setParent_uuid(String parent_uuid) {
		this.parent_uuid = parent_uuid;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

}
