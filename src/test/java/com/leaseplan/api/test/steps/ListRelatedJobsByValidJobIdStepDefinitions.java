package com.leaseplan.api.test.steps;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;

import com.leaseplan.api.test.steps.dto.JobRelatedJob;
import com.leaseplan.api.test.steps.dto.JobRelatedJobs;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class ListRelatedJobsByValidJobIdStepDefinitions {
	Response jobresponse = null;
	RequestSpecification request = null;

	@Given("a valid job_id {string} for the related jobs")
	public void given_listing_related_jobs(String id) {
		request = RestAssured.given();

	}

	@When("calling the related jobs end point with a valid job_id {string}")
	public void when_call_list_related_jobs(String id) {
		jobresponse = request.get("http://api.dataatwork.org/v1/jobs/"+ id +"/related_jobs");
	}

	@Then("the response should list the jobs related to the given valid job_id {string}")
	public void then_validate_response_body(String id) {
		assertThat(jobresponse.statusCode()).isEqualTo(200);
		assertThat(jobresponse.contentType()).isEqualTo("application/json");
		JobRelatedJobs relatedjobs = jobresponse.as(JobRelatedJobs.class);
		// Asserting response is not null and mapped to the JobRelatedJobs class
		assertThat(relatedjobs).isNotNull();
		ArrayList<JobRelatedJob> jobrelatedjob = relatedjobs.getRelated_job_titles();
		JobRelatedJob firstRelatedJob = jobrelatedjob.get(0);
		// Asserting the first element in the list has the same UUID as the input
		assertThat(firstRelatedJob.getUuid()).isEqualTo(id);
		
		for(int i=0;i<jobrelatedjob.size();i++) {
			// Asserting that all the related jobs have the same parent UUID
			assertThat(jobrelatedjob.get(i).getParent_uuid()).isEqualTo(firstRelatedJob.getParent_uuid());
		}
		
	}

}
