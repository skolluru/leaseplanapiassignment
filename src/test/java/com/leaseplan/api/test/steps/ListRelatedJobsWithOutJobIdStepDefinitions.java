package com.leaseplan.api.test.steps;

import static org.assertj.core.api.Assertions.assertThat;

import com.leaseplan.api.test.steps.dto.JobError;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class ListRelatedJobsWithOutJobIdStepDefinitions {
	Response jobresponse = null;
	RequestSpecification request = null;

	@Given("a blank job_id for the related jobs")
	public void given_listing_related_jobs_blank_job_id() {
		request = RestAssured.given();

	}

	@When("calling the related jobs end point with a blank job_id")
	public void when_call_list_related_jobs_blank_job_id() {
		jobresponse = request.get(" http://api.dataatwork.org/v1/jobs/related_jobs");
	}

	@Then("the response should through an error message Cannot find job with id related_jobs")
	public void then_validate_response_body() {
		assertThat(jobresponse.statusCode()).isEqualTo(404);
		assertThat(jobresponse.contentType()).isEqualTo("application/json");
		
		JobError message = jobresponse.as(JobError.class);
		assertThat(message.getError().getMessage()).isEqualTo("Cannot find job with id related_jobs");
	}

}
