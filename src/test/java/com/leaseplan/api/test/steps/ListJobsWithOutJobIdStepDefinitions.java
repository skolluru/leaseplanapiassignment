package com.leaseplan.api.test.steps;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;

import com.leaseplan.api.test.steps.dto.Job;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class ListJobsWithOutJobIdStepDefinitions {
	Response jobresponse = null;
	RequestSpecification request = null;

	@Given("API is available for listing jobs")
	public void given_api_listing_jobs() {
		request = RestAssured.given();

	}

	@When("calling the list jobs end point")
	public void when_call_list_jobs() {
		jobresponse = request.get("http://api.dataatwork.org/v1/jobs");
	}

	@Then("the response status code should be OK")
	public void then_validate_response_code() {
		assertThat(jobresponse.statusCode()).isEqualTo(200);
		assertThat(jobresponse.contentType()).isEqualTo("application/json");
		assertThat(jobresponse.as(ArrayList.class)).isNotNull();
	}

}
